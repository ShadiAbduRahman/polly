/*
  ==============================================================================

    MidiOutputPort.h
    Created: 9 Jul 2014 11:14:43am
    Author:  Shadi Abdu-Rahman

  ==============================================================================
*/

#ifndef MIDIOUTPUTPORT_H_INCLUDED
#define MIDIOUTPUTPORT_H_INCLUDED

#include "JuceHeader.h"
#include "Identifiers.h"
//==============================================================================

class MidiOutputPort
{
public:
    //==============================================================================
    MidiOutputPort(ValueTree midiOutputPort, MidiOutput* midiOutput = nullptr);
    ~MidiOutputPort();
    
    String getName();
    void sendMessage(const MidiMessage& midiMessage);
    void bufferMessage(const MidiMessage& midiMessage, int sampleNumber);
    void sendBufferedMessages(double sampleRate);
    void incUsers();
    void decUsers();
    bool unused();
    
private:
    //==============================================================================
    void clearBufferedMessages();
    void start();
    void stop();

    ValueTree midiOutputPort;
    ScopedPointer<MidiOutput> midiOutput;
    MidiBuffer midiBuffer;
    
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MidiOutputPort)
};


#endif  // MIDIOUTPUTPORT_H_INCLUDED
