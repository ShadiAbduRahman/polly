/*
  ==============================================================================

    Identifiers.h
    Created: 14 May 2014 6:18:37pm
    Author:  Shadi Abdu-Rahman

  ==============================================================================
*/

#ifndef IDENTIFIERS_H_INCLUDED
#define IDENTIFIERS_H_INCLUDED

static const Identifier projectType("PROJECT");
static const Identifier transportType("TRANSPORT");
static const Identifier midiInputType("MIDI_INPUT");
static const Identifier midiOutputType("MIDI_OUTPUT");
static const Identifier midiInputsType("MIDI_INPUTS");
static const Identifier midiOutputsType("MIDI_OUTPUTS");
static const Identifier midiIOType("MIDI_IO");
static const Identifier midiProcessorType("MIDI_PROCESSOR");
static const Identifier name("name");
static const Identifier id("id");
static const Identifier internal("internal");
static const Identifier active("active");
static const Identifier useCount("useCount");
static const Identifier bpm("bpm");

#endif  // IDENTIFIERS_H_INCLUDED
