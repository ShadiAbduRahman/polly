/*
  ==============================================================================

    MidiProcessor.h
    Created: 14 May 2014 1:32:59pm
    Author:  Shadi Abdu-Rahman

  ==============================================================================
*/

#ifndef MIDIPROCESSOR_H_INCLUDED
#define MIDIPROCESSOR_H_INCLUDED

#include "JuceHeader.h"
#include "Identifiers.h"
#include "MidiOutputPort.h"
//==============================================================================

class MidiProcessor
{
    
public:
    //==============================================================================
    MidiProcessor();
    virtual ~MidiProcessor();
    
    virtual void start() = 0;
    virtual void stop() = 0;
    virtual void pause() = 0;
    virtual void unpause() = 0;
    virtual void process(const double ppqPosition, const double sampleRate) = 0;
    
    //TODO:
    //void addMidiInput(MidiInputPort* port);
    //void removeMidiInput(const int portIndex);
    
    void addMidiOutput(MidiOutputPort* port);
    void removeMidiOutput(const int portIndex);
    
protected:
    //==============================================================================
    double last_ppqPosition = 0.0f;
    
    //TODO:
    //Array<MidiInputPort*> midiInputs;
    
    Array<MidiOutputPort*> midiOutputs;
    UndoManager* undoManager;
    
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MidiProcessor)
};

#endif  // MIDIPROCESSOR_H_INCLUDED
