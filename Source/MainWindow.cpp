/*
  ==============================================================================

    MainWindow.cpp
    Created: 28 Apr 2014 5:11:52pm
    Author:  Shadi Abdu-Rahman

  ==============================================================================
*/

#include "MainWindow.h"
//==============================================================================

class CommandIDs
{
public:
    //==============================================================================
    static const int create                 = 0x10000;
    static const int open                   = 0x10001;
    static const int save                   = 0x10002;
    static const int saveAs                 = 0x10003;
    static const int about                  = 0x10004;
    static const int undo                   = 0x10100;
    static const int redo                   = 0x10101;
    static const int add                    = 0x10102;
    static const int prefs                  = 0x10103;
};


//==============================================================================

int i = 0;

MainWindow::MainWindow(ApplicationProperties* appProperties, ApplicationCommandManager* commandManager): DocumentWindow(ProjectInfo::projectName, Colours::lightgrey, DocumentWindow::allButtons), appProperties(appProperties), commandManager(commandManager)
{
    //TODO: Support opening registered project (and other) file types (see transportNode e.g.). But for now:
    ValueTree projectNode(projectType);
    project = new Project();
    project->setProjectNode(projectNode);
    audioDeviceManager.addAudioCallback(project->getTransport());
    
    audioDeviceSelector = new AudioDeviceSelectorComponent(audioDeviceManager, 0, 0, 1, 1, false, false, true, false);
    //TODO: Handle case if there's no previous audioDevice saved and case when saved device isn't found.
    audioDeviceState = appProperties->getUserSettings()->getXmlValue("audioDeviceState");
    audioDeviceManager.initialise(0, 1, audioDeviceState, false);
    
    addKeyListener(this->commandManager->getKeyMappings());
    
    //TODO?: setApplicationCommandManagerToWatch(commandManager);
    
    commandManager->registerAllCommandsForTarget(this);
    
    #if JUCE_MAC
        PopupMenu* menu = new PopupMenu();
        menu->addCommandItem(commandManager, CommandIDs::about);
        menu->addSeparator();
        menu->addCommandItem(commandManager, CommandIDs::prefs);
        menu->addSeparator();
        setMacMainMenu(this, menu);
        delete menu;
    #else
        setMenuBar(this);
    #endif
    
    this->commandManager->setFirstCommandTarget(this);
    
    Process::setPriority (Process::HighPriority);
    
    setContentOwned(mainContent = new MainContentComponent(project), true);
    
    setUsingNativeTitleBar(true);
    setResizable (true, true);
    centreWithSize(getWidth(), getHeight());
    restoreWindowPosition();
    setVisible(true);
}

MainWindow::~MainWindow()
{
    //TODO: Ask user to save any unsaved changes
    audioDeviceManager.removeAudioCallback(project->getTransport());
    project->close();
    
    appProperties->getUserSettings()->setValue("lastMainWindowPos", getWindowStateAsString());
    audioDeviceState = audioDeviceManager.createStateXml();
    appProperties->getUserSettings()->setValue("audioDeviceState", audioDeviceState);
    appProperties->saveIfNeeded();
	removeKeyListener(commandManager->getKeyMappings());
    
    #if JUCE_MAC
        setMacMainMenu(nullptr);
    #else
        setMenuBar(nullptr);
    #endif
}

void MainWindow::restoreWindowPosition()
{
    String windowState;
    
    if (windowState.isEmpty())
        windowState = appProperties->getUserSettings()->getValue("lastMainWindowPos");
    
    restoreWindowStateFromString (windowState);
}

void MainWindow::closeButtonPressed()
{
    JUCEApplication::getInstance()->systemRequestedQuit();
}

StringArray MainWindow::getMenuBarNames()
{
    const char* const names[] = {"File", "Edit", nullptr};
    return StringArray(names);
}

PopupMenu MainWindow::getMenuForIndex(int topLevelMenuIndex, const String& menuName)
{
    PopupMenu menu;
    
    switch (topLevelMenuIndex)
    {
        case 0:
            // "File" menu
            menu.addCommandItem(commandManager, CommandIDs::create);
            menu.addCommandItem(commandManager, CommandIDs::open);
            menu.addCommandItem(commandManager, CommandIDs::save);
            menu.addCommandItem(commandManager, CommandIDs::saveAs);
            #if !JUCE_MAC
                menu.addSeparator();
                menu.addCommandItem(commandManager, StandardApplicationCommandIDs::about);
                menu.addSeparator();
                menu.addCommandItem(commandManager, StandardApplicationCommandIDs::quit);
            #endif
            break;
            
        case 1:
            // "Edit" menu
            menu.addCommandItem(commandManager, CommandIDs::undo);
            menu.addCommandItem(commandManager, CommandIDs::redo);
            menu.addCommandItem(commandManager, CommandIDs::add);
            menu.addSeparator();
            #if !JUCE_MAC
                menu.addSeparator();
                menu.addCommandItem(commandManager, CommandIDs::prefs);
            #endif
            break;
    }
    return menu;
}

void MainWindow::menuItemSelected(int menuItemID, int topLevelMenuIndex)
{
    //TODO
}

ApplicationCommandTarget* MainWindow::getNextCommandTarget()
{
    return findFirstTargetParentComponent();
}

void MainWindow::getAllCommands(Array <CommandID>& commands)
{
    const CommandID ids[] = {CommandIDs::create, CommandIDs::open, CommandIDs::save, CommandIDs::saveAs, CommandIDs::about, CommandIDs::undo, CommandIDs::redo, CommandIDs::add, CommandIDs::prefs};
    commands.addArray (ids, numElementsInArray(ids));
}

void MainWindow::getCommandInfo(const CommandID commandID, ApplicationCommandInfo& result)
{
    const StringArray category = getMenuBarNames();
    
    switch (commandID)
    {
        case CommandIDs::create:
            result.setInfo("New", "Create a New Project", category[0], 0);
            result.defaultKeypresses.add(KeyPress('n', ModifierKeys::commandModifier, 0));
            break;
        case CommandIDs::open:
            result.setInfo("Open Project...", "Open a Project", category[0], 0);
            result.defaultKeypresses.add(KeyPress('o', ModifierKeys::commandModifier, 0));
            break;
            
        case CommandIDs::save:
            result.setInfo("Save Project", "Save the Current Project", category[0], 0);
            result.defaultKeypresses.add(KeyPress('s', ModifierKeys::commandModifier, 0));
            break;
            
        case CommandIDs::saveAs:
            result.setInfo("Save Project As...", "Save the Current Project to a New Project File", category[0], 0);
            result.defaultKeypresses.add(KeyPress('s', ModifierKeys::shiftModifier | ModifierKeys::commandModifier, 0));
            break;
            
        case CommandIDs::about:
            result.setInfo("About", "About" + appProperties->getStorageParameters().applicationName, category[0], 0);
            result.defaultKeypresses.add(KeyPress('a', ModifierKeys::commandModifier, 0));
            break;
            
        case CommandIDs::undo:
            result.setInfo("Undo", "Undo Last Change", category[1], 0);
            result.defaultKeypresses.add(KeyPress('z', ModifierKeys::commandModifier, 0));
            break;
            
        case CommandIDs::redo:
            result.setInfo("Redo", "Redo Last Change", category[1], 0);
            result.defaultKeypresses.add(KeyPress('z', ModifierKeys::shiftModifier | ModifierKeys::commandModifier, 0));
            break;
            
        case CommandIDs::add:
            result.setInfo("Add", "Add Item", category[1], 0);
            result.defaultKeypresses.add(KeyPress('+', ModifierKeys::commandModifier, 0));
            break;
            
        case CommandIDs::prefs:
            result.setInfo("Preferences...", "Edit Preferences", category[1], 0);
            result.defaultKeypresses.add(KeyPress(',', ModifierKeys::commandModifier, 0));
            break;
            
        default:
            break;
    }
}

bool MainWindow::perform(const InvocationInfo& info)
{
    switch (info.commandID)
    {
        case CommandIDs::create:
            createNewProject();
            break;
            
        case CommandIDs::open:
            // TODO
            break;
            
        case CommandIDs::save:
            // TODO
            break;
            
        case CommandIDs::saveAs:
            // TODO
            break;
            
        case CommandIDs::about:
            // TODO
            break;
            
        case CommandIDs::undo:
            // TODO
            break;
            
        case CommandIDs::redo:
            // TODO
            break;
            
        case CommandIDs::add:
            // TODO
            break;
            
        case CommandIDs::prefs:
            showPreferencesDialog(prefOwner);
            break;
            
        default:
            return false;
    }
    return true;
}

void MainWindow::showPreferencesDialog (ScopedPointer<Component>& ownerPointer)
{
    if (ownerPointer != nullptr)
        ownerPointer->toFront (true);
    else
        new Dialog(appName + " Preferences", "lastPrefDialogPos", new Preferences(appProperties, audioDeviceSelector), ownerPointer, appProperties, false, 820, 800);
}

void MainWindow::closeProject()
{
    //TODO: Ask user to save any unsaved changes
    createNewProject();
}

void MainWindow::createNewProject()
{
    audioDeviceManager.removeAudioCallback(project->getTransport());
    ValueTree projectNode(projectType);
    project = new Project();
    project->setProjectNode(projectNode);
    mainContent->setProject(project);
    audioDeviceManager.addAudioCallback(project->getTransport());
}
