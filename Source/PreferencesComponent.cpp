
#include "PreferencesComponent.h"
//==============================================================================

Preferences::Preferences(ApplicationProperties* appProperties, AudioDeviceSelectorComponent* audioDeviceSelector) : appProperties(appProperties)
{
    //TODO: Add more tabs when needed
    addAndMakeVisible(tabbedComponent = new TabbedComponent(TabbedButtonBar::TabsAtTop));
    tabbedComponent->setTabBarDepth(30);
    tabbedComponent->addTab("Audio Interface", Colours::lightgrey, audioDeviceSelector, false, -1);
    tabbedComponent->setCurrentTabIndex(0);
}

Preferences::~Preferences()
{
}

void Preferences::paint (Graphics& g)
{
    g.fillAll(Colours::darkgrey);
}

void Preferences::resized()
{
    tabbedComponent->setBounds(proportionOfWidth (0.5000f) - ((proportionOfWidth (0.9500f)) / 2), proportionOfHeight (0.5000f) - ((proportionOfHeight (0.9500f)) / 2), proportionOfWidth (0.9500f), proportionOfHeight (0.9500f));
}

