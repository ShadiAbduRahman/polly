
#ifndef PREFERENCES_COMPONENT_H_INCLUDED
#define PREFERENCES_COMPONENT_H_INCLUDED

#include "JuceHeader.h"
#include "MidiDeviceScanner.h"
//==============================================================================

class Preferences  : public Component
{
    
public:
    //==============================================================================
    Preferences(ApplicationProperties* appProperties, AudioDeviceSelectorComponent* audioDeviceSelector);
    ~Preferences();
    
    void paint (Graphics& g);
    void resized();

private:
    //==============================================================================
    ApplicationProperties* appProperties;
    ScopedPointer<TabbedComponent> tabbedComponent;
    
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (Preferences)
};

#endif   // PREFERENCES_COMPONENT_H_INCLUDED
