/*
  ==============================================================================

    Dialog.cpp
    Created: 30 Apr 2014 3:26:54pm
    Author:  Shadi Abdu-Rahman

  ==============================================================================
*/

#include "Dialog.h"
//==============================================================================

Dialog::Dialog (const String& title, const String& windowPosPropertyName, Component* content, ScopedPointer<Component>& ownerPointer, ApplicationProperties* appProperties,
                bool resizable, int defaultW, int defaultH, bool resizeableLimits, int minW, int minH, int maxW, int maxH):
                DialogWindow(title, Colours::lightgrey, true, true), windowPosProperty(windowPosPropertyName), owner(ownerPointer), appProperties(appProperties), resizable(resizable)
{
    setUsingNativeTitleBar (true);
    setResizable (resizable, true);
    if (resizable) {
        if (resizeableLimits)
            setResizeLimits (minW, minH, maxW, maxH);
        
        const String windowState(appProperties->getUserSettings()->getValue(windowPosProperty));
        
        if (windowState.isNotEmpty())
            restoreWindowStateFromString(windowState);
        else
            centreAroundComponent(Component::getCurrentlyFocusedComponent(), defaultW, defaultH);
    }
    else
        centreAroundComponent(Component::getCurrentlyFocusedComponent(), defaultW, defaultH);
    setContentOwned (content, false);
    setVisible (true);
    owner = this;
}

Dialog::~Dialog()
{
    if (resizable) {
        appProperties->getUserSettings()->setValue(windowPosProperty, getWindowStateAsString());
        appProperties->saveIfNeeded();
    }
}

void Dialog::closeButtonPressed()
{
    owner = nullptr;
}