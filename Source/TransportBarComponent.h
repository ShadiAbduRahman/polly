#ifndef __TRANSPORT_BAR_COMPONENT__
#define __TRANSPORT_BAR_COMPONENT__

#include "JuceHeader.h"
#include "Identifiers.h"
#include "Transport.h"
#include "MidiClock.h"
#include "MidiIO.h"
//==============================================================================

class TransportBarComponent  : public Component,
                               public ComboBoxListener,
                               public ButtonListener,
                               public SliderListener
{
public:
    //==============================================================================
    TransportBarComponent(Transport* transport, UndoManager* undoManager, MidiIO* midiIO);
    ~TransportBarComponent();

    void paint(Graphics& g);
    void resized();
    void comboBoxChanged(ComboBox* comboBoxThatHasChanged);
    void buttonClicked(Button* buttonThatWasClicked);
    void sliderValueChanged(Slider* sliderThatWasMoved);

private:
    //==============================================================================
    //TODO: Move to MidiProcessorComponent super class
    StringArray midiInDevices, midiOutDevices;
    Transport* transport;
    ScopedPointer<MidiProcessor> midiProcessor;
    UndoManager* undoManager;
    MidiIO* midiIO;
    
    bool started = false;
    
    ScopedPointer<ComboBox> midiClockInCB;
    ScopedPointer<TextButton> startPauseButton;
    ScopedPointer<TextButton> stopButton;
    ScopedPointer<Slider> bpmSlider;
    ScopedPointer<TextButton> addPortButton, removePortButton;
    ScopedPointer<PopupMenu> addPortMenu, removePortMenu;

    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (TransportBarComponent)
};

#endif   // __TRANSPORT_BAR_COMPONENT__
