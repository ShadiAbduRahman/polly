/*
  ==============================================================================

    MidiProcessor.cpp
    Created: 14 May 2014 1:32:59pm
    Author:  Shadi Abdu-Rahman

  ==============================================================================
*/

#include "MidiProcessor.h"
//==============================================================================
MidiProcessor::MidiProcessor()
{
}

MidiProcessor::~MidiProcessor()
{
    //TODO:
    //midiInputs.clear();
    midiOutputs.clear();
}

/** TODO:
void MidiProcessor::addMidiInput(MidiInput* port)
{
    midiInputs.addIfNotAlreadyThere(port);
}

void MidiProcessor::removeMidiInput(const int portIndex)
{
    midiInputs.remove(portIndex);
}
**/

void MidiProcessor::addMidiOutput(MidiOutputPort* port)
{
    midiOutputs.addIfNotAlreadyThere(port);
}

void MidiProcessor::removeMidiOutput(const int portIndex)
{
    midiOutputs.remove(portIndex);
}