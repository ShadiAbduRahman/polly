/*
  ==============================================================================

    MidiIO.cpp
    Created: 8 Jul 2014 1:40:21pm
    Author:  Shadi Abdu-Rahman

  ==============================================================================
*/

#include "MidiIO.h"

MidiIO::MidiIO()
{
    ValueTree miio(midiIOType);
    //TODO:
    //midiInputs = new OwnedArray<MidiInput>();
    
    midiOutputs = new OwnedArray<MidiOutputPort>();
}

MidiIO::~MidiIO()
{
}

/** TODO:
MidiInputPort* MidiIO::addMidiInput(const ValueTree mi)
{
    
}

void MidiIO::removeMidiInput(const ValueTree mi)
{
    
}
**/

MidiOutputPort* MidiIO::addMidiOutput(const ValueTree mo)
{
    String moName = mo.getProperty(name);
    
    for(MidiOutputPort** output = midiOutputs->begin(); output != midiOutputs->end(); ++output)
        if (moName == (*output)->getName())
        {
            (*output)->incUsers();
            return *output;
        }
    
    MidiOutputPort* output;
    ValueTree midiOutput = mo.createCopy();
    if (virtualPortsEnabled() && midiOutput.getProperty(internal))
        output = new MidiOutputPort(midiOutput, MidiOutput::createNewDevice(moName));
    else
        output = new MidiOutputPort(midiOutput, MidiOutput::openDevice(mo.getProperty(id)));
    output->incUsers();
    midiOutputs->add(output);
    return output;
}

void MidiIO::removeMidiOutput(const ValueTree mo)
{
    String moName = mo.getProperty(name);
    MidiOutputPort** output;
    for(output = midiOutputs->begin(); output != midiOutputs->end(); ++output)
        if (moName == (*output)->getName())
            break;
    (*output)->decUsers();
    if ((*output)->unused())
        midiOutputs->removeObject(*output);
}

bool MidiIO::virtualPortsEnabled()
{
    #if JUCE_MAC
        return true;
    #endif
    return false;
}
