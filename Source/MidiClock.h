/*
  ==============================================================================

    MidiClock.h
    Created: 11 May 2014 5:32:55pm
    Author:  Shadi Abdu-Rahman

  ==============================================================================
*/

#ifndef MIDICLOCK_H_INCLUDED
#define MIDICLOCK_H_INCLUDED

#include "JuceHeader.h"
#include "MidiProcessor.h"
//==============================================================================

class MidiClock : public MidiProcessor
{
    
public:
    //==============================================================================
    MidiClock();
    ~MidiClock();
    
    /** Implements MidiProcessor methods*/
    void start();
    void stop();
    void pause();
    void unpause();
    void process(const double ppqPosition, const double sampleRate);
    
private:
    //==============================================================================    
    ScopedPointer<MidiMessage> startMessage = new MidiMessage(MidiMessage::midiStart());
    ScopedPointer<MidiMessage> continueMessage = new MidiMessage(MidiMessage::midiContinue());
    ScopedPointer<MidiMessage> stopMessage = new MidiMessage(MidiMessage::midiStop());
    ScopedPointer<MidiMessage> clockMessage = new MidiMessage(MidiMessage::midiClock());
    
    const int PPQ_INTERVAL = 40;
    
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MidiClock)
};


#endif  // MIDICLOCK_H_INCLUDED
