/*
  ==============================================================================

    Dialog.h
    Created: 30 Apr 2014 3:26:54pm
    Author:  Shadi Abdu-Rahman

  ==============================================================================
*/

#ifndef DIALOG_H_INCLUDED
#define DIALOG_H_INCLUDED

#include "JuceHeader.h"
//==============================================================================

class Dialog : public DialogWindow
{
public:
    //==============================================================================
    Dialog(const String& title, const String& windowPosPropertyName, Component* content, ScopedPointer<Component>& ownerPointer, ApplicationProperties* appProperties, bool resizable,
            int defaultW, int defaultH, bool resizeableLimits= false, int minW = 800, int minH = 600, int maxW = 800, int maxH = 600);
    
    ~Dialog();
    
    void closeButtonPressed() override;
    
private:
    //==============================================================================
    String windowPosProperty;
    ScopedPointer<Component>& owner;
    ApplicationProperties* appProperties;
    bool resizable;
    
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (Dialog)
};

#endif  // DIALOG_H_INCLUDED
