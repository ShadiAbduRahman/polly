/*
  ==============================================================================

    Project.h
    Created: 14 May 2014 9:43:06pm
    Author:  Shadi Abdu-Rahman

  ==============================================================================
*/

#ifndef PROJECT_H_INCLUDED
#define PROJECT_H_INCLUDED

#include "JuceHeader.h"
#include "Identifiers.h"
#include "Transport.h"
#include "MidiProcessor.h"
//==============================================================================

class Project
{
    
public:
    //==============================================================================
    Project();
    ~Project();
    
    void close();
    Transport* getTransport();
    ValueTree getProjectNode();
    void setProjectNode(ValueTree projectNode);
    
private:
    //==============================================================================
    ValueTree projectNode;
    ScopedPointer<Transport> transport;
    
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (Project)
};



#endif  // PROJECT_H_INCLUDED
