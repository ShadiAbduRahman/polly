#include "TransportBarComponent.h"
//==============================================================================

TransportBarComponent::TransportBarComponent(Transport* transport, UndoManager* undoManager, MidiIO* midiIO) : transport(transport), undoManager(undoManager), midiIO(midiIO)
{
    midiProcessor = new MidiClock();
    
    addAndMakeVisible(midiClockInCB = new ComboBox("Midiclock In"));
    midiClockInCB->setTooltip(TRANS("Choose between internal and external midi clock by selecting \"Internal\" or a Midi In Port."));
    midiClockInCB->setEditableText(false);
    midiClockInCB->setJustificationType (Justification::centredLeft);
    midiClockInCB->setTextWhenNothingSelected (String::empty);
    midiClockInCB->setTextWhenNoChoicesAvailable (String::empty);
    midiClockInCB->addItem (TRANS("Internal"), 1);
    midiClockInCB->addListener(this);
    
    addAndMakeVisible(bpmSlider = new Slider ("BPM"));
    bpmSlider->setRange(20, 999, 0.01);
    bpmSlider->setSliderStyle(Slider::IncDecButtons);
    bpmSlider->setTextBoxStyle(Slider::TextBoxLeft, false, 80, 30);
    bpmSlider->setColour(Slider::thumbColourId, Colours::white);
    bpmSlider->setColour(Slider::rotarySliderFillColourId, Colour (0xffffffff));
    bpmSlider->setColour(Slider::textBoxHighlightColourId, Colour (0xffffffff));
    bpmSlider->setValue(transport->getTransportNode().getProperty(bpm));
    bpmSlider->addListener(this);

    addAndMakeVisible(startPauseButton = new TextButton("Start Pause Button"));
    startPauseButton->setButtonText(TRANS("Start"));
    startPauseButton->addListener(this);
    startPauseButton->setColour(TextButton::buttonColourId, Colours::white);
    startPauseButton->setColour(TextButton::buttonOnColourId, Colours::white);

    addAndMakeVisible(stopButton = new TextButton("Stop Button"));
    stopButton->setButtonText(TRANS("Stop"));
    stopButton->addListener(this);
    stopButton->setColour(TextButton::buttonColourId, Colours::white);
    stopButton->setColour(TextButton::buttonOnColourId, Colours::white);
    stopButton->setEnabled(false);

    addAndMakeVisible(addPortButton = new TextButton ("Add Out Port"));
    addPortButton->setButtonText (TRANS("Add Out Port ..."));
    addPortButton->addListener(this);
    addPortButton->setColour (TextButton::buttonColourId, Colours::white);
    addPortButton->setColour (TextButton::buttonOnColourId, Colours::white);
    
    addAndMakeVisible(removePortButton = new TextButton ("Remove Out Port"));
    removePortButton->setButtonText (TRANS("Remove Out Port ..."));
    removePortButton->addListener(this);
    removePortButton->setColour (TextButton::buttonColourId, Colours::white);
    removePortButton->setColour (TextButton::buttonOnColourId, Colours::white);
    
    addPortMenu = new PopupMenu();
    removePortMenu = new PopupMenu();

    setSize (1280, 50);
}

TransportBarComponent::~TransportBarComponent()
{
    PopupMenu::dismissAllActiveMenus();
    midiClockInCB = nullptr;
    bpmSlider = nullptr;
    startPauseButton = nullptr;
    stopButton = nullptr;
    addPortButton = nullptr;
    removePortButton = nullptr;
}

//==============================================================================
void TransportBarComponent::paint (Graphics& g)
{
    g.fillAll (Colour (0xff5f5f5f));
}

void TransportBarComponent::resized()
{
    midiClockInCB->setBounds (10, proportionOfHeight (0.5000f) - ((proportionOfHeight (0.8000f)) / 2), 140, proportionOfHeight (0.8000f));
    bpmSlider->setBounds ((10) + (140) - -10, proportionOfHeight (0.5000f) - ((proportionOfHeight (0.8000f)) / 2), 150, proportionOfHeight (0.8000f));
    startPauseButton->setBounds (proportionOfWidth (0.4850f) - ((50) / 2), proportionOfHeight (0.5000f) - ((proportionOfHeight (0.8000f)) / 2), 50, proportionOfHeight (0.8000f));
    stopButton->setBounds ((proportionOfWidth (0.4850f) - ((50) / 2)) + (50) - -10, proportionOfHeight (0.5000f) - ((proportionOfHeight (0.8000f)) / 2), 50, proportionOfHeight (0.8000f));
    removePortButton->setBounds (getWidth() - 10 - 140, proportionOfHeight (0.5000f) - ((proportionOfHeight (0.8000f)) / 2), 140, proportionOfHeight (0.8000f));
    addPortButton->setBounds (getWidth() - 20 - 280 , proportionOfHeight (0.5000f) - ((proportionOfHeight (0.8000f)) / 2), 140, proportionOfHeight (0.8000f));
}

void TransportBarComponent::comboBoxChanged (ComboBox* comboBoxThatHasChanged)
{
    if (comboBoxThatHasChanged == midiClockInCB)
    {
        //TODO
    }
}

void TransportBarComponent::sliderValueChanged (Slider* sliderThatWasMoved)
{
    if (sliderThatWasMoved == bpmSlider)
    {
        transport->getTransportNode().setProperty(bpm, bpmSlider->getValue(), undoManager);
    }
}

void TransportBarComponent::buttonClicked (Button* buttonThatWasClicked)
{
    if (buttonThatWasClicked == startPauseButton)
    {
        if (started)
        {
            started = false;
            startPauseButton->setButtonText("Start");
            transport->pause();
        }
        else
        {
            started = true;
            startPauseButton->setButtonText("Pause");
            transport->start();
        }
        stopButton->setEnabled(true);
    }
    else if (buttonThatWasClicked == stopButton)
    {
        started = false;
        startPauseButton->setButtonText("Start");
        stopButton->setEnabled(false);
        transport->stop();
    }
    else if (buttonThatWasClicked == addPortButton)
    {
        addPortMenu->clear();
        midiOutDevices = MidiOutput::getDevices();
        ValueTree transportNode = transport->getTransportNode();
        ValueTree selected = transportNode.getChildWithName(midiOutputsType);
        ValueTree available(midiOutputsType);
        
        if (midiIO->virtualPortsEnabled())
        {
            ValueTree mo(midiOutputType);
            mo.setProperty(name, "Polly Output", nullptr);
            mo.setProperty(id, -1, nullptr);
            mo.setProperty(internal, true, nullptr);
            mo.setProperty(active, !selected.getChildWithProperty(name, "Polly Output").isValid(), nullptr);
            available.addChild(mo, -1, nullptr);
            
            for (String* it = midiOutDevices.begin(); it != midiOutDevices.end(); ++it)
            {
                if (it->compare("Polly Input") != 0)
                {
                    ValueTree mo(midiOutputType);
                    mo.setProperty(name, *it, nullptr);
                    mo.setProperty(id, int(it - midiOutDevices.begin()), nullptr);
                    mo.setProperty(internal, false, nullptr);
                    mo.setProperty(active, !selected.getChildWithProperty(name, *it).isValid(), nullptr);
                    available.addChild(mo, -1, nullptr);
                }
            }
        }
        else
        {
            for (String* it = midiOutDevices.begin(); it != midiOutDevices.end(); ++it)
            {
                ValueTree mo(midiOutputType);
                mo.setProperty(name, *it, nullptr);
                mo.setProperty(id, int(it - midiOutDevices.begin()), nullptr);
                mo.setProperty(internal, false, nullptr);
                mo.setProperty(active, !selected.getChildWithProperty(name, *it).isValid(), nullptr);
                available.addChild(mo, -1, nullptr);
            }
        }
        
        for (int i = 0; i < available.getNumChildren(); ++i)
        {
            ValueTree mo = available.getChild(i);
            addPortMenu->addItem(i + 1, mo.getProperty(name), mo.getProperty(active), false);
        }
        
        int result = addPortMenu->show();
        if (result != 0)
        {
            ValueTree mo = available.getChild(result - 1).createCopy();
            mo.setProperty(active, true, nullptr);
            selected.addChild(mo, -1, undoManager);
            MidiOutputPort* mop = midiIO->addMidiOutput(mo);
            midiProcessor->addMidiOutput(mop);
            transport->addMidiProcessor(midiProcessor);
        }
    }
    
    else if (buttonThatWasClicked == removePortButton)
    {
        removePortMenu->clear();
        midiOutDevices = MidiOutput::getDevices();
        ValueTree transportNode = transport->getTransportNode();
        ValueTree selected = transportNode.getChildWithName(midiOutputsType);
        
        int numChildren = selected.getNumChildren();
        for (int i = 0; i < numChildren; ++i)
        {
            ValueTree mo = selected.getChild(i);
            if (midiOutDevices.contains(mo.getProperty(name).toString()) || mo.getProperty(internal))
                removePortMenu->addItem(i + 1, mo.getProperty(name), true, false);
        }
        
        int result = removePortMenu->show();
        if (result != 0)
        {
            ValueTree mo = selected.getChild(result - 1);
            selected.removeChild(result - 1, undoManager);
            midiIO->removeMidiOutput(mo);
            midiProcessor->removeMidiOutput(result - 1);
        }
    }
}
