#include "MainComponent.h"
//==============================================================================

MainContentComponent::MainContentComponent(Project* project) : project(project)
{
    undoManager = new UndoManager();
    midiIO = new MidiIO();
    
    parseTree();
    
    setSize(1280, 702);
}

MainContentComponent::~MainContentComponent()
{
}

void MainContentComponent::paint(Graphics& g)
{
    g.fillAll(Colours::whitesmoke);
    g.setColour(Colours::black);
}

void MainContentComponent::resized()
{
    content->setBounds(0, 30, proportionOfWidth(1.0f), proportionOfHeight(1.0f) - 40);
    contentPort->setBounds(0, 30, proportionOfWidth(1.0f), proportionOfHeight(1.0f) - 30);
    transportBar->setBounds(0, 0, proportionOfWidth(1.0f), 30);
}

void MainContentComponent::setProject(Project* project)
{
    this->project = project;
    parseTree();
}

void MainContentComponent::parseTree()
{
    addAndMakeVisible(transportBar = new TransportBarComponent(project->getTransport(), undoManager, midiIO));
    addAndMakeVisible(content = new ContentComponent());
    content->setSize(getWidth(), 410);
    addAndMakeVisible(contentPort = new Viewport());
    contentPort->setScrollBarsShown(true, true);
    contentPort->setViewedComponent(content, false);
    /*
     TODO: Iterate over all contained nodes in projectNode to create corresponding objects
     */
}