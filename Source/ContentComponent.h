/*
  ==============================================================================

    ContentComponent.h
    Created: 18 May 2014 12:40:08am
    Author:  Shadi Abdu-Rahman

  ==============================================================================
*/

#ifndef CONTENTCOMPONENT_H_INCLUDED
#define CONTENTCOMPONENT_H_INCLUDED

#include "JuceHeader.h"
#include "MidiSequenceComponent.h"
//==============================================================================

class ContentComponent : public Component, ButtonListener
{
public:
    //==============================================================================
    ContentComponent();
    ~ContentComponent();

    void paint (Graphics&);
    void resized();
    int getSeqCount() {return seqCount;};
    void buttonClicked(Button* buttonThatWasClicked);

private:
    //==============================================================================
    ScopedPointer<TextButton> addSeqButton;
    OwnedArray<MidiSequenceComponent> midiSeqs;
    MidiSequenceComponent* msc;
    
    int seqCount = 0;
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (ContentComponent)
};


#endif  // CONTENTCOMPONENT_H_INCLUDED
