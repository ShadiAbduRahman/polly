/*
  ==============================================================================

    ContentComponent.cpp
    Created: 18 May 2014 12:40:08am
    Author:  Shadi Abdu-Rahman

  ==============================================================================
*/

#include "JuceHeader.h"
#include "ContentComponent.h"

//==============================================================================
ContentComponent::ContentComponent()
{
    addAndMakeVisible(addSeqButton = new TextButton ("+"));
    addSeqButton->setButtonText(TRANS(" + "));
    addSeqButton->addListener(this);
    addSeqButton->setColour(TextButton::buttonColourId, Colours::white);
    addSeqButton->setColour(TextButton::buttonOnColourId, Colours::white);
}

ContentComponent::~ContentComponent()
{
    midiSeqs.clear();
}

void ContentComponent::paint (Graphics& g)
{
    g.fillAll(Colours::whitesmoke);
    g.setColour(Colours::black);
    //g.drawText("Width: " + String(getLocalBounds().getWidth()) + "; Height: " + String(getLocalBounds().getHeight()), getLocalBounds(), Justification::centred, true);
}

void ContentComponent::resized()
{
    addSeqButton->setBounds(proportionOfWidth(0.5000f) - ((45) / 2), 5 + (seqCount * 355), 45, 45);
    //TODO: Resize Midi Seqs, etc
}

void ContentComponent::buttonClicked(Button* buttonThatWasClicked)
{
    if (buttonThatWasClicked == addSeqButton)
    {
        addAndMakeVisible(msc = new MidiSequenceComponent());
        msc->setBounds(5, 5 + (seqCount * 355), 800, 350);
        ++seqCount;
        midiSeqs.add(msc);
        addSeqButton->setBounds(proportionOfWidth(0.5000f) - ((45) / 2), 5 + (seqCount * 355), 45, 45);
        setSize(getParentComponent()->getWidth(), 410 * seqCount + 1);
    }
}
