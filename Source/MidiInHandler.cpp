/*
  ==============================================================================

    MidiInHandler.cpp
    Created: 1 May 2014 6:22:13pm
    Author:  Shadi Abdu-Rahman

  ==============================================================================
*/

#include "MidiInHandler.h"
//==============================================================================

void MidiInHandler::handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message)
{
    if (message.isMidiClock()) {
        
        if (numPulses == -1) {
            lastPulseTime = message.getTimeStamp();
            ++numPulses;
        }
        else
            if (++numPulses == NUM_PULSES) {
                double newPulseTime = message.getTimeStamp();
                double bpm = 60 / (newPulseTime - lastPulseTime);
                if (NUM_BPMS > 1)
                {
                    bpmValues.insert(0, bpm);
                    if (bpmValues.size() > NUM_BPMS)
                        bpmValues.removeLast();
                    double bpmSum;
                    for (int i = 0; i < bpmValues.size(); ++i)
                        bpmSum += bpmValues.getUnchecked(i);
                    
                    bpm = bpmSum / bpmValues.size();
                }
                //TODO: Remove
                //DBG (floorf(bpm * 100 + 0.5) / 100);
                numPulses = 0;
                lastPulseTime = newPulseTime;
            }
    }
    
    else if (message.isMidiStart()) {
        DBG ("Midiclock start");
        numPulses = -1;
    }
    
    else if (message.isMidiStop()) {
        DBG ("Midiclock Stop");
        numPulses = -1;
    }
    
    else if (message.isMidiContinue()) {
        DBG ("Midiclock Continue");
        numPulses = -1;
    }
    
}
