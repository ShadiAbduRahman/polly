/*
  ==============================================================================

    MidiClock.cpp
    Created: 11 May 2014 5:32:55pm
    Author:  Shadi Abdu-Rahman

  ==============================================================================
*/

#include "MidiClock.h"
//==============================================================================

MidiClock::MidiClock()
{
}

MidiClock::~MidiClock()
{
}

void MidiClock::start()
{
    for(MidiOutputPort** output = midiOutputs.begin(); output != midiOutputs.end(); ++output)
        (*output)->sendMessage(*startMessage);
}

void MidiClock::stop()
{
    pause();
    last_ppqPosition = 0.0f;
}

void MidiClock::pause()
{
    for(MidiOutputPort** output = midiOutputs.begin(); output != midiOutputs.end(); ++output)
        (*output)->sendMessage(*stopMessage);
}

void MidiClock::unpause()
{
    for(MidiOutputPort** output = midiOutputs.begin(); output != midiOutputs.end(); ++output)
        (*output)->sendMessage(*continueMessage);
}

void MidiClock::process(const double ppqPosition, const double sampleRate)
{
    double current_ppqPosition = last_ppqPosition;
    if (ppqPosition > current_ppqPosition) {
        current_ppqPosition += PPQ_INTERVAL;
        for(MidiOutputPort** output = midiOutputs.begin(); output != midiOutputs.end(); ++output)
            (*output)->sendMessage(*clockMessage);
    }
    last_ppqPosition = current_ppqPosition;
}