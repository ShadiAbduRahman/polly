/*
  ==============================================================================

    MidiIO.h
    Created: 8 Jul 2014 1:40:21pm
    Author:  Shadi Abdu-Rahman

  ==============================================================================
*/

#ifndef MIDIIO_H_INCLUDED
#define MIDIIO_H_INCLUDED

#include "JuceHeader.h"
#include "Identifiers.h"
#include "MidiOutputPort.h"
//==============================================================================

class MidiIO
{
public:
    //==============================================================================
    MidiIO();
    ~MidiIO();
    
    //TODO:
    //MidiInputPort* addMidiInput(const ValueTree mi);
    //void removeMidiInput(const ValueTree mi);
    
    MidiOutputPort* addMidiOutput(const ValueTree mo);
    void removeMidiOutput(const ValueTree mo);
    
    bool virtualPortsEnabled();
    
private:
    //==============================================================================
    //TODO:
    //ScopedPointer<OwnedArray<MidiInputPort>>midiInputs;
    
    ScopedPointer<OwnedArray<MidiOutputPort>> midiOutputs;
    
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MidiIO)
};

#endif  // MIDIIO_H_INCLUDED
