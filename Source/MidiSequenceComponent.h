#ifndef __MIDI_SEQUENCE_COMPONENT__
#define __MIDI_SEQUENCE_COMPONENT__

#include "JuceHeader.h"
//==============================================================================

class MidiSequenceComponent :   public Component,
                                public ButtonListener,
                                public SliderListener
{
public:
    //==============================================================================
    MidiSequenceComponent ();
    ~MidiSequenceComponent();

    void paint (Graphics& g);
    void resized();
    void buttonClicked(Button* buttonThatWasClicked);
    void sliderValueChanged(Slider* sliderThatWasMoved);

private:
    //==============================================================================
    ScopedPointer<Viewport> seqPort;
    ScopedPointer<TextButton> addSourceButton, removeSourceButton, addTargetButton, removeTargetButton;
    
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MidiSequenceComponent)
};

#endif   // __MIDI_SEQUENCE_COMPONENT__
