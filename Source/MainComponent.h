#ifndef MAINCOMPONENT_H_INCLUDED
#define MAINCOMPONENT_H_INCLUDED

#include "JuceHeader.h"
#include "Identifiers.h"
#include "TransportBarComponent.h"
#include "Project.h"
#include "MidiIO.h"
#include "ContentComponent.h"
//==============================================================================

class MainContentComponent : public Component
{
public:
    //==============================================================================
    MainContentComponent(Project* project);
    ~MainContentComponent();

    void paint(Graphics&);
    void resized();
    
    void setProject(Project* project);

private:
    //==============================================================================
    void parseTree();
    ScopedPointer<UndoManager> undoManager;
    Project* project;

    ScopedPointer<TransportBarComponent> transportBar;
    ScopedPointer<MidiIO> midiIO;
    ScopedPointer<ContentComponent> content;
    ScopedPointer<Viewport> contentPort;
    
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainContentComponent)
};


#endif  // MAINCOMPONENT_H_INCLUDED
