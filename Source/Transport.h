/*
  ==============================================================================

    Transport.h
    Created: 7 May 2014 7:48:31pm
    Author:  Shadi Abdu-Rahman

  ==============================================================================
*/

#ifndef TRANSPORT_H_INCLUDED
#define TRANSPORT_H_INCLUDED

#include "JuceHeader.h"
#include "Identifiers.h"
#include "MidiProcessor.h"
//==============================================================================

class Transport : public AudioIODeviceCallback
{
    
public:
    //==============================================================================
    Transport(ValueTree projectNode);
    ~Transport();
    
    void start();
    void pause();
    void stop();
    
    void addMidiProcessor(MidiProcessor* midiProcessor);
    void removeMidiProcessor(MidiProcessor* midiProcessor);
    
    ValueTree getTransportNode();
    
    /** Implement AudioIODeviceCallback methods */
    void audioDeviceIOCallback(const float** inputChannelData, int numInputChannels, float** outputChannelData, int numOutputChannels, int numSamples);
    void audioDeviceAboutToStart(AudioIODevice* device);
    void audioDeviceStopped();
    void audioDeviceError(const String& errorMessage);
    
private:
    //==============================================================================
    void process(int numSamples);
    ValueTree transportNode;
    
    volatile bool isPlaying = false, isPaused = false, isStopped = true;
    int64 samplePosition = 0;
    double ppqPosition = 0.0f;
    double sampleDuration;
    double ppqDuration;
    
    int64 ppqPositionInteger = 0;
    volatile int bar = 1, _4thBeat = 1, _16thBeat = 1;
    
    double sampleRate;
    int bufferSize;
    
    Array<MidiProcessor*> midiProcessors;
    
    static const uint TRANSPORT_PPQ_RESOLUTION = 960;
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (Transport)
};

#endif  // TRANSPORT_H_INCLUDED
