#include "JuceHeader.h"
#include "MainWindow.h"
//==============================================================================

class Polly  : public JUCEApplication
{
public:
    //==============================================================================
    Polly() {}

    const String getApplicationName()       { return appName; }
    const String getApplicationVersion()    { return appVersion; }
    bool moreThanOneInstanceAllowed()       { return false; }

    //==============================================================================
    void initialise (const String& commandLine)
    {
        
        PropertiesFile::Options options;
        options.folderName          = "Transienta";
        options.applicationName     = appName;
        options.filenameSuffix      = "settings";
        options.osxLibrarySubFolder = "Application Support";
        
        //TODO: ?? options.storageFormat       = PropertiesFile::storeAsCompressedBinary;
        
        appProperties = new ApplicationProperties();
        appProperties->setStorageParameters(options);
        
        commandManager = new ApplicationCommandManager();
        //TODO: ?? commandManager->registerAllCommandsForTarget(this);
        
        LookAndFeel::setDefaultLookAndFeel(&lookAndFeel);
        
        mainWindow = new MainWindow(appProperties, commandManager);
    }

    void shutdown()
    {
        LookAndFeel::setDefaultLookAndFeel(nullptr);
        mainWindow = nullptr;
        commandManager = nullptr;
    }

    //==============================================================================
    void systemRequestedQuit()
    {
        shutdown();
        quit();
    }

    //==============================================================================

private:
    String appName = ProjectInfo::projectName;
    String appVersion = ProjectInfo::versionString;
    
    LookAndFeel_V3 lookAndFeel;
    ScopedPointer<MainWindow> mainWindow;
    ScopedPointer<ApplicationProperties> appProperties;
    ScopedPointer<ApplicationCommandManager> commandManager;
};

//==============================================================================

START_JUCE_APPLICATION(Polly)
