/*
  ==============================================================================

    MidiOutputPort.cpp
    Created: 9 Jul 2014 11:14:43am
    Author:  Shadi Abdu-Rahman

  ==============================================================================
*/

#include "MidiOutputPort.h"

MidiOutputPort::MidiOutputPort(ValueTree midiOutputPort, MidiOutput* midiOutput) : midiOutputPort(midiOutputPort), midiOutput(midiOutput)
{
    start();
}

MidiOutputPort::~MidiOutputPort()
{
    clearBufferedMessages();
    stop();
}

String MidiOutputPort::getName()
{
    return midiOutputPort.getProperty(name);
}

void MidiOutputPort::sendMessage(const MidiMessage& midiMessage)
{
    midiOutput->sendMessageNow(midiMessage);
}

void MidiOutputPort::bufferMessage(const MidiMessage& midiMessage, int sampleNumber)
{
    midiBuffer.addEvent(midiMessage, sampleNumber);
}

void MidiOutputPort::sendBufferedMessages(double sampleRate)
{
    midiOutput->sendBlockOfMessages(midiBuffer, Time::getMillisecondCounterHiRes(), sampleRate);
}

void MidiOutputPort::clearBufferedMessages()
{
    midiOutput->clearAllPendingMessages();
}

void MidiOutputPort::start()
{
    midiOutput->startBackgroundThread();
}

void MidiOutputPort::stop()
{
    midiOutput->stopBackgroundThread();
}

void MidiOutputPort::incUsers()
{
    midiOutputPort.setProperty(useCount, int(midiOutputPort.getProperty(useCount)) + 1, nullptr);
}

void MidiOutputPort::decUsers()
{
    midiOutputPort.setProperty(useCount, int(midiOutputPort.getProperty(useCount)) - 1, nullptr);
}

bool MidiOutputPort::unused()
{
    return int(midiOutputPort.getProperty(useCount)) == 0;
}
