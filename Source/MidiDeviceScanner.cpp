/*
  ==============================================================================

    MidiDeviceScanner.cpp
    Created: 1 May 2014 10:10:19am
    Author:  Shadi Abdu-Rahman

  ==============================================================================
*/

#include "MidiDeviceScanner.h"
//==============================================================================


MidiDeviceScanner::MidiDeviceScanner(): Thread("Midi Device Scanner")
{
}

MidiDeviceScanner::~MidiDeviceScanner()
{
    if (this->isThreadRunning())
        stop();
}

void MidiDeviceScanner::run()
{
    while (!threadShouldExit() && missingMidiPortsInUse)
    {
        
        String* it;
        foundMidiInDevices = MidiInput::getDevices();
        if (midiInDevices != foundMidiInDevices)
        {
            midiInDevices = foundMidiInDevices;
            for (it = midiInDevices.begin(); it != midiInDevices.end(); ++it)
                DBG ("Midi Inputs = " + *it);
        }
            
        foundMidiOutDevices = MidiOutput::getDevices();
        if (midiOutDevices != foundMidiOutDevices)
        {
            midiOutDevices = foundMidiOutDevices;
            for (it = midiOutDevices.begin(); it != midiOutDevices.end(); ++it)
                DBG ("Midi Outputs = " + *it);
        }
        wait(TIME_TO_WAIT);
    }
}

void MidiDeviceScanner::start()
{
    startThread(SCANNER_PRIO);
}

bool MidiDeviceScanner::stop()
{
    return stopThread(TIME_TO_WAIT);
}

void MidiDeviceScanner::setMissingMidiPortsInUse(bool isMissing)
{
    //TODO: Critical Section??
    missingMidiPortsInUse = isMissing;
}