/*
  ==============================================================================

    MidiInHandler.h
    Created: 1 May 2014 6:22:13pm
    Author:  Shadi Abdu-Rahman

  ==============================================================================
*/

#ifndef MIDIINHANDLER_H_INCLUDED
#define MIDIINHANDLER_H_INCLUDED

#include "JuceHeader.h"
//==============================================================================

class MidiInHandler : public MidiInputCallback
{
public:
    //==============================================================================
    MidiInHandler() {};
    
    //TODO:
    void handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message);
    
private:
    //==============================================================================
    double lastPulseTime;
    int numPulses = -1;
    const int NUM_PULSES = 96;
    Array<double> bpmValues;
    const int NUM_BPMS = 1;
    
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MidiInHandler)
};



#endif  // MIDIINHANDLER_H_INCLUDED
