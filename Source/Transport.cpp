/*
  ==============================================================================

    Transport.cpp
    Created: 7 May 2014 7:48:31pm
    Author:  Shadi Abdu-Rahman

  ==============================================================================
*/

#include "Transport.h"
#include <stdlib.h>
//==============================================================================

Transport::Transport(ValueTree projectNode) : transportNode(projectNode.getChildWithName(transportType))
{
    if (!transportNode.isValid())
    {
        ValueTree tt(transportType);
        transportNode = tt;
        ValueTree mis(midiInputsType);
        ValueTree mos(midiOutputsType);
        tt.addChild(mis, -1, nullptr);
        tt.addChild(mos, -1, nullptr);
        transportNode.setProperty(bpm, 120.0f, nullptr);
        projectNode.addChild(transportNode, 0, nullptr);
    }
    else
    {
        //TODO: parseTree
    }
}

Transport::~Transport()
{
}

void Transport::start()
{
    isPlaying = true;
    if (isPaused)
    {
        isPaused = false;
        for(MidiProcessor** proc = midiProcessors.begin(); proc != midiProcessors.end(); ++proc)
            (*proc)->unpause();
    }
    else if (isStopped) {
        isStopped = false;
        for(MidiProcessor** proc = midiProcessors.begin(); proc != midiProcessors.end(); ++proc)
            (*proc)->start();
    }
}

void Transport::pause()
{
    if (isPlaying)
    {
        isPlaying = false;
        isPaused = true;
        isStopped = false;
        for(MidiProcessor** proc = midiProcessors.begin(); proc != midiProcessors.end(); ++proc)
            (*proc)->pause();
    }
}

void Transport::stop()
{
    ppqPosition = 0;
    bar = 1, _4thBeat = 1, _16thBeat = 1;
    isPlaying = false;
    isPaused = false;
    isStopped = true;
    for(MidiProcessor** proc = midiProcessors.begin(); proc != midiProcessors.end(); ++proc)
        (*proc)->stop();
}

void Transport::addMidiProcessor(MidiProcessor* midiProcessor)
{
    midiProcessors.addIfNotAlreadyThere(midiProcessor);
    
}

void Transport::removeMidiProcessor(MidiProcessor* midiProcessor)
{        
    midiProcessors.removeFirstMatchingValue(midiProcessor);
}

ValueTree Transport::getTransportNode()
{
    return transportNode;
}

/* From AudioIODeviceCallback */
void Transport::audioDeviceIOCallback(const float** inputChannelData, int numInputChannels, float** outputChannelData, int numOutputChannels, int numSamples)
{
    if (isPlaying && midiProcessors.size())
        process(numSamples);
}

void Transport::audioDeviceAboutToStart(AudioIODevice* device)
{
    sampleRate = device->getCurrentSampleRate();
    bufferSize = device->getCurrentBufferSizeSamples();
}

void Transport::audioDeviceStopped()
{
    stop();
}

void Transport::audioDeviceError(const String& errorMessage)
{
    stop();
    DBG(errorMessage);
}

void Transport::process(int numSamples)
{
    //Update Timeline
    samplePosition += numSamples;
    sampleDuration = numSamples / sampleRate;
    ppqDuration = 60.0f / ((double) transportNode.getProperty(bpm) * TRANSPORT_PPQ_RESOLUTION);
    ppqPosition += sampleDuration / ppqDuration;
    
    for(MidiProcessor** proc = midiProcessors.begin(); proc != midiProcessors.end(); ++proc)
        (*proc)->process(ppqPosition, sampleRate);
    
    //TODO: Notify GUI listeners (Bar Transport)
    ppqPositionInteger = (int64) ppqPosition;
    _16thBeat = (ppqPositionInteger % TRANSPORT_PPQ_RESOLUTION) / 240 + 1;
    _4thBeat = ppqPositionInteger % (TRANSPORT_PPQ_RESOLUTION * 4) / TRANSPORT_PPQ_RESOLUTION + 1;
    bar = ppqPositionInteger / (TRANSPORT_PPQ_RESOLUTION * 4) + 1;
    
    //sstd::cout << bar << ":" << _4thBeat << ":" << _16thBeat << ": " << ppqPosition << '\n';
}
