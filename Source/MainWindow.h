/*
  ==============================================================================

    MainWindow.h
    Created: 28 Apr 2014 5:11:52pm
    Author:  Shadi Abdu-Rahman

  ==============================================================================
*/

#ifndef MAINWINDOW_H_INCLUDED
#define MAINWINDOW_H_INCLUDED

#include "JuceHeader.h"
#include "Identifiers.h"
#include "MainComponent.h"
#include "PreferencesComponent.h"
#include "Dialog.h"
#include "Project.h"
#include "Transport.h"

class MainWindow:   public DocumentWindow,
                    public MenuBarModel,
                    public ApplicationCommandTarget
{
public:
    //==============================================================================
    MainWindow(ApplicationProperties* appProperties, ApplicationCommandManager* commandManager);
    ~MainWindow();
    
    void restoreWindowPosition();
    void closeButtonPressed();
    void changeListenerCallback(ChangeBroadcaster*);
    
    //bool isInterestedInFileDrag (const StringArray& files);
    //void fileDragEnter (const StringArray& files, int, int);
    //void fileDragMove (const StringArray& files, int, int);
    //void fileDragExit (const StringArray& files);
    //void filesDropped (const StringArray& files, int, int);
    
    StringArray getMenuBarNames();
    PopupMenu getMenuForIndex (int topLevelMenuIndex, const String& menuName);
    void menuItemSelected (int menuItemID, int topLevelMenuIndex);
    
    ApplicationCommandTarget* getNextCommandTarget();
    void getAllCommands (Array<CommandID>& commands);
    void getCommandInfo (CommandID commandID, ApplicationCommandInfo& result);
    bool perform (const InvocationInfo& info);
    
private:
    //==============================================================================
    void showPreferencesDialog(ScopedPointer<Component>& ownerPointer);
    void closeProject();
    void createNewProject();
    
    ApplicationProperties* appProperties;
    ApplicationCommandManager* commandManager;
    String appName = ProjectInfo::projectName;
    ScopedPointer<Component> prefOwner;
    
    ScopedPointer<AudioDeviceSelectorComponent> audioDeviceSelector;
    AudioDeviceManager audioDeviceManager;
    ScopedPointer<XmlElement> audioDeviceState;
    ScopedPointer<Project> project;
    ScopedPointer<MainContentComponent> mainContent;
    
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(MainWindow)
};


#endif  // MAINWINDOW_H_INCLUDED
