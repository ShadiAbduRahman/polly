#include "MidiSequenceComponent.h"
//==============================================================================
MidiSequenceComponent::MidiSequenceComponent()
{
    addAndMakeVisible(seqPort = new Viewport ("Seq Port"));
    seqPort->setScrollBarsShown (false, true);
    
    addAndMakeVisible(addSourceButton = new TextButton ("Add Source"));
    addSourceButton->setButtonText (TRANS("Add Source ..."));
    addSourceButton->addListener(this);
    addSourceButton->setColour (TextButton::buttonColourId, Colours::white);
    addSourceButton->setColour (TextButton::buttonOnColourId, Colours::white);
    
    addAndMakeVisible(removeSourceButton = new TextButton ("Remove Source"));
    removeSourceButton->setButtonText (TRANS("Remove Source ..."));
    removeSourceButton->addListener(this);
    removeSourceButton->setColour (TextButton::buttonColourId, Colours::white);
    removeSourceButton->setColour (TextButton::buttonOnColourId, Colours::white);
    
    addAndMakeVisible(addTargetButton = new TextButton ("Add Target"));
    addTargetButton->setButtonText (TRANS("Add Target ..."));
    addTargetButton->addListener(this);
    addTargetButton->setColour (TextButton::buttonColourId, Colours::white);
    addTargetButton->setColour (TextButton::buttonOnColourId, Colours::white);
    
    addAndMakeVisible(removeTargetButton = new TextButton ("Remove Target"));
    removeTargetButton->setButtonText (TRANS("Remove Target ..."));
    removeTargetButton->addListener(this);
    removeTargetButton->setColour (TextButton::buttonColourId, Colours::white);
    removeTargetButton->setColour (TextButton::buttonOnColourId, Colours::white);
    setSize(800, 350);
}

MidiSequenceComponent::~MidiSequenceComponent()
{
    seqPort = nullptr;
}

//==============================================================================
void MidiSequenceComponent::paint (Graphics& g)
{
    g.fillAll (Colour (0xff626262));
}

void MidiSequenceComponent::resized()
{
    seqPort->setBounds(5, getHeight() + 50, 600, 300);
    
    removeSourceButton->setBounds (10, proportionOfHeight (0.0833f) - ((proportionOfHeight (0.0667f)) / 2), 140, proportionOfHeight (0.0667f));
    addSourceButton->setBounds (160, proportionOfHeight (0.0833f) - ((proportionOfHeight (0.0667f)) / 2), 140, proportionOfHeight (0.0667f));
    
    removeTargetButton->setBounds (getWidth() - 10 - 140, proportionOfHeight (0.0833f) - ((proportionOfHeight (0.0667f)) / 2), 140, proportionOfHeight (0.0667f));
    addTargetButton->setBounds (getWidth() - 20 - 280, proportionOfHeight (0.0833f) - ((proportionOfHeight (0.0667f)) / 2), 140, proportionOfHeight (0.0667f));
}

void MidiSequenceComponent::buttonClicked(Button* buttonThatWasClicked)
{
}
void MidiSequenceComponent::sliderValueChanged(Slider* sliderThatWasMoved)
{
}