/*
  ==============================================================================

    MidiDeviceScanner.h
    Created: 1 May 2014 10:10:19am
    Author:  Shadi Abdu-Rahman

  ==============================================================================
*/

#ifndef MIDIDEVICESCANNER_H_INCLUDED
#define MIDIDEVICESCANNER_H_INCLUDED

#include "JuceHeader.h"
//==============================================================================

class MidiDeviceScanner : private Thread
{
    
public:
    //==============================================================================
    MidiDeviceScanner();
    ~MidiDeviceScanner();
    
    void start();
    bool stop();
    
    void setMissingMidiPortsInUse(bool isMissing);

private:
    //==============================================================================
    
    void run();
    const int TIME_TO_WAIT = 1000;
    const int SCANNER_PRIO = 0;
    bool volatile missingMidiPortsInUse = false;
    
    StringArray midiInDevices, midiOutDevices, foundMidiInDevices, foundMidiOutDevices;
    
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MidiDeviceScanner)
    
};


#endif  // MIDIIOSCANTHREAD_H_INCLUDED
