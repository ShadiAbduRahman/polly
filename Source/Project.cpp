/*
  ==============================================================================

    Project.cpp
    Created: 14 May 2014 9:43:06pm
    Author:  Shadi Abdu-Rahman

  ==============================================================================
*/

#include "Project.h"
//==============================================================================

Project::Project()
{
}

Project::~Project()
{
}

void Project::close()
{
    //TODO: Any cleanup before destruction
}

Transport* Project::getTransport()
{
    return transport;
}

ValueTree Project::getProjectNode()
{
    return projectNode;
}

void Project::setProjectNode(ValueTree projectNode)
{
    this->projectNode = projectNode;
    transport = new Transport(projectNode);
}